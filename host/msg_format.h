/* 
* Author: Ahren Sitar
* Date: April, 11 2013
* Purpose:This code is to set up the the message format that will be sent between
* the LM4F120H5QR device and the Host PC using USB CDC protocol
* 
* Rev       Author      Details
* ------------------------------
* 1.0       A.S         Create msg structure with 3 members all type int fields
*                       first member is the heartbeat signal that will let the
*                       LM4F120H5QR know that the Host PC is still alive.
*/
#include <sys/stat.h>
#ifndef Msg_h
#define Msg_h

typedef struct {
    int Memory;
    int CPU;
    int UpTime;    
}Msg;

Msg packet;

#endif