/* 
* Author: Ahren Sitar
* Date: April, 11 2013
* Purpose:This code is to set up to test sending the msg structure to the
*           LM4F120H5QR device.  This test will communicate locally to a fifo
*           file named LM4F at location,
*           /home/campus07/aasitar/Desktop/EE/EE_4735/Final_Project/ahrens_code/
* 
* Rev       Author      Details
* ------------------------------
* 1.0       A.S         Creating the packaging for the Msg structure
* 1.1		A.S			Adding packege information to Msg structure to send
* 1.2		A.S			Revision on adding mem, uptime, and cpu usage to packet
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "msg_format.h"

int main() {
    
    /* variable declaration and initialization */
    FILE* fp;
	Msg packet;
	double up, mem;
	long double a[4],b[4],loadavg;
	int i, fd;
    
    /* set heartbeat of system to anything but 0 and period of heartbeat */
    unsigned period = 3;
    
    /* create file descriptor to LM4F fifo file */
    fd = open("/dev/ttyACM1", O_WRONLY);
    
    /* keep sending packet at every "period" seconds */
    while(1) {
		
		/* UP TIME */
		fp = fopen ("/proc/uptime", "r");
		if (fp == NULL) {
			printf("Failed to run command\n" );
			return 1;
		}    
		fscanf (fp, "%lf", &up);
		packet.UpTime = up;
		printf("Up Time: %d\n", packet.UpTime);
		fclose (fp);
		
		/* FREE MEMORY */
		fp = fopen ("/proc/meminfo", "r");
		if (fp == NULL) {
			printf("Failed to run command\n" );
			return 1;
		}    
		i = 37; // offset into /proc/meminfo
		fseek(fp, i, SEEK_SET);
		fscanf (fp, "%lf", &mem);
		packet.Memory = mem;
		printf("Free Memory: %d\n", packet.Memory);
		fclose (fp);
		
		/* CPU USAGE */
		fp = fopen("/proc/stat","r");
		if (fp == NULL) {
			printf("Failed to run command\n" );
			return 1;
		}
		fscanf(fp,"%*s %Lf %Lf %Lf %Lf",&a[0],&a[1],&a[2],&a[3]);
		for(i=0; i<=3; i++){
			printf("a[%d]: %Lf\n", i, a[i]);
		}
		fclose(fp);
		
		// Sleep to allow /proc/stat to update
		sleep(1);
		
		fp = fopen("/proc/stat","r");
		if (fp == NULL) {
			printf("Failed to run command\n" );
			return 1;
		}
		fscanf(fp,"%*s %Lf %Lf %Lf %Lf",&b[0],&b[1],&b[2],&b[3]);
		for(i=0; i<=3; i++){
			printf("b[%d]: %Lf\n", i, b[i]);
		}
		fclose(fp);

		packet.CPU = 100*(((b[0]+b[1]+b[2]) - (a[0]+a[1]+a[2])) / ((b[0]+b[1]+b[2]+b[3]) - (a[0]+a[1]+a[2]+a[3])));
        printf("The current CPU utilization is %u%%\n", packet.CPU);
        
		/* write to LM4F fifo file */
		write(fd, &packet, sizeof(packet));
		sleep(period);
    }
    
}
