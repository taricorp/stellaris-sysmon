
/*
 * Spawns a new worker process, returning a read-only handle to its standard
 * output stream.
 *
 * Returns -1 on error and sets errno accordingly.
 *
 * Workers must not read stdin, and write status reports to stdout. stderr
 * is assumed to be logged.
 */
int spawn_worker(const char *binary, int *pid) {
    int p[2];
    if (pipe(p) == -1) {
        return -1;
    }

    pid_t cpid;
    if ((cpid = fork()) == 0) {
        // Worker process
        close(STDIN_FILENO);
        // Stdout attaches to pipe
        dup2(p[1], STDOUT_FILENO);
        close(p[0]);
        close(p[1]);
        // Re-exec
        execl(binary);
        perror("worker_spawn");
        exit(1);
    } else {
        // Discard write end
        close(p[1]);
    }

    if (pid != NULL)
        *pid = cpid;
    return p[0];
}
