#include <stdio.h>
#include <stdlib.h>

#include <pty.h>
#include <pwd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

struct shell {
    // PID of child process
    pid_t pid;
    // Master side
    int pty;
};

const char *getLoginShell(const uid_t uid) {
    struct passwd *pwent = getpwuid(uid);
    return pwent->pw_shell;
}

int spawnPTY(struct shell *s, const char *binary, char **argv) {
    if ((s->pid = forkpty(&s->pty, NULL, NULL, NULL)) == 0) {
        // Child process
        execvp(binary, argv);
        perror("execvp");
        exit(1);
    } else if (s->pid == -1) {
        // forkpty() failed
        return -1;
    }
    return 0;
}

int main(int argc, char **argv) {
    int amaster;

    pid_t cpid;
    if ((cpid = forkpty(&amaster, NULL, NULL, NULL)) == 0) {
        // Execute user's shell in PTY
        const char *shell = getLoginShell(getuid());
        execl(shell, shell, "-l", NULL);
        //execl("/bin/login", "/bin/login", NULL, NULL);
        perror("exec");
        exit(1);
    }
    fprintf(stderr, "Child PID %i\n", cpid);

    // Blocking I/O on both channels, so we must select() to handle both
    // directions.
    fd_set channels;
    unsigned char c;

    while (1) {
        FD_ZERO(&channels);
        FD_SET(amaster, &channels);
        FD_SET(STDIN_FILENO, &channels);

        if (select(FD_SETSIZE, &channels, NULL, NULL, NULL) < 0) {
            perror("select");
            return 1;
        }

        if (FD_ISSET(amaster, &channels)) {
            if (read(amaster, &c, 1) != 1 || write(STDOUT_FILENO, &c, 1) != 1) {
                fprintf(stderr, "Disconnected\n");
                return 0;
            }
        }
        if (FD_ISSET(STDIN_FILENO, &channels)) {
            if (read(STDIN_FILENO, &c, 1) != 1 || write(amaster, &c, 1) != 1) {
                fprintf(stderr, "Disconnected\n");
                return 0;
            }
        }
    }
}
