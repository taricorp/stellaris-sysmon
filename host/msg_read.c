/* 
* Author: Ahren Sitar
* Date: April, 11 2013
* Purpose:This code is to set up to test reading the msg structure to the
*           LM4F120H5QR device.  This test will read the local fifo file named
*           LM4F at location,
*           /home/campus07/aasitar/Desktop/EE/EE_4735/Final_Project/ahrens_code/
* 
* Rev       Author      Details
* ------------------------------
* 1.0       A.S         
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include "msg_format.h"

int main() {
    
    /* variable declaration and initialization */
    Msg packet;
    int fd, period, r;
    r = 0;
    /* set heartbeat of system to anything but 0 and period of heartbeat */
    period = 3;
    
    /* create file descriptor to LM4F fifo file */
    fd = open("LM4F", O_RDWR);
    
    /* keep sending heartbeat at every "period" seconds */
    while(r == 0) {        
        /* read from LM4F fifo file */       
        r = read(fd, &packet, sizeof(int)*7);
        
        /* print out packet info */
        printf("Free Memory: %d\n", packet.Memory);
        printf("CPU Usage: %d\%\n", packet.CPU);
        printf("UpTime: %d\n", packet.UpTime);
        
        sleep(period);
    }
    
}