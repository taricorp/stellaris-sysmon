#include <stdint.h>

struct time {
    unsigned utime;
    uint8_t days;
    uint8_t hours;
    uint8_t minutes;
    uint8_t seconds;
};

void rtc_init();

void rtc_get(struct time *);

// RTC tick ISR
void rtc_timer_tick() __attribute__((interrupt("IRQ")));
