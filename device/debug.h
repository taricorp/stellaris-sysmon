#include <stdint.h>

void debug_init();

void debug_putc(const char c);
void debug_puts(const char *s);
void debug_printf(const char *fmt, ...);

unsigned char debug_getc();
unsigned int debug_getHex();
