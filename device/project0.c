#include <inc/hw_types.h>
#include <inc/hw_memmap.h>
#include <driverlib/rom.h>
#include <driverlib/sysctl.h>
#include <driverlib/gpio.h>

#include "rtc.h"
#include "ui.h"

void powerButton_init() {
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
    ROM_GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_3|GPIO_PIN_2);
}

void powerButton_press(unsigned milliseconds) {
    ROM_GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_3, GPIO_PIN_3);
    ROM_SysCtlDelay((50000000 / 3000) * milliseconds);
    ROM_GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_3, 0);
}

unsigned volatile seconds = 0;
unsigned volatile lastUpdate = 0;
unsigned watchdogEnabled = 1;

int main(void) {
    // 50 MHz core clock from PLL via 16 MHz crystal
    ROM_SysCtlClockSet(SYSCTL_SYSDIV_4|SYSCTL_USE_PLL|SYSCTL_XTAL_16MHZ|
                       SYSCTL_OSC_MAIN);

    ui_init();
    cdc_init();
    rtc_init();
    powerButton_init();

    unsigned i = 0;
    while (1) {
        // Dirty hack for not-really-real-time timing
        if (++i == 102400 * 7) {
            seconds++;
            unsigned delta = seconds - lastUpdate;
            if (delta > 30 && watchdogEnabled) {
                powerButton_press(5000);    // Power off
                ROM_SysCtlDelay(50000000);  // Wait 1 second
                powerButton_press(500);     // Power on
                // Wait for another timeout before rebooting again
                lastUpdate = seconds;
            }
            i = 0;
            ui_redraw();
        }
        ui_process();
    }
    return 0;
}
