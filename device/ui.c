#include <stdint.h>

#include <inc/hw_types.h>
#include <inc/hw_memmap.h>
#include <driverlib/gpio.h>
#include <driverlib/rom.h>
#include <driverlib/sysctl.h>
#include <driverlib/uart.h>

#include "rtc.h"

// Current system time and time of last packet received
extern volatile unsigned seconds, lastUpdate;
// Is the watchdog enabled?
extern unsigned watchdogEnabled;

// Free memory in kilobytes
static volatile unsigned memoryFree = 0;
// CPU usage (% * 100)
static volatile unsigned cpuUsage = 0;
// Uptime in seconds
static volatile unsigned uptime = 0;

void ui_init() {
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
    ROM_GPIOPinConfigure(GPIO_PA0_U0RX);
    ROM_GPIOPinConfigure(GPIO_PA1_U0TX);
    ROM_GPIOPinTypeUART(GPIO_PORTA_BASE, 3);
    ROM_UARTClockSourceSet(UART0_BASE, UART_CLOCK_SYSTEM);
    ROM_UARTConfigSetExpClk(UART0_BASE,
                            ROM_SysCtlClockGet(),
                            115200,
                            UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE);
    ROM_UARTEnable(UART0_BASE);
}

/*
 * Write 'c' to the UI.
 */
static void ui_putc(unsigned char c) {
    ROM_UARTCharPut(UART0_BASE, c);
}

/*
 * Simply emit the string 's' to the UI device.
 */
static void ui_puts_raw(const char *s) {
    while (*s) {
        ui_putc(*s++);
    }
}

/*
 * Write the string representation of 'x' to the UI.
 */
static void ui_putInt(unsigned x) {
    int log10 = 0; // Number of digits neeeded
    int xt = x;
    while (xt > 0) {
        log10++;
        xt /= 10;
    }

    do { // Defer comparison to ensure 0 gets a digit
        int xpow = 1, i;
        for (i = log10; i > 1; i--) { // 10^log10
            xpow *= 10;
        }
        char digit = (x / xpow) % 10;
        digit += '0';
        ui_putc(digit);
    } while (--log10 > 0);
}

/*
 * Write the string 's' to the UI at terminal coordinates (x,y).
 */
static void ui_puts(unsigned x, unsigned y, const char *s) {
    // VT100 control codes
    ui_puts_raw("\x1b[");
    ui_putInt(y);
    ui_putc(';');
    ui_putInt(x);
    ui_putc('H');
    ui_puts_raw(s);
}

/*
 * Clears the terminal.
 */
static void ui_clearScreen() {
    ui_puts_raw("\x1b[2J");
}

// UI modes
enum {
    UI_MENU,
    UI_SHELL,
};
static unsigned uiState = UI_MENU;
static unsigned menuCursor = 1;

const char *const menuOptions[] = {
    "Shutdown host",
    "Reboot host",
    "Power on host",
    "Disable watchdog",
    "Enable watchdog"
};

/*
 * Display the main menu/status screen.
 */
void ui_redrawMenu() {
    unsigned up, cpu, mem, delta;
    // Atomic reads
    ROM_IntMasterDisable();
    up = uptime;
    cpu = cpuUsage;
    mem = memoryFree;
    delta = seconds - lastUpdate;
    ROM_IntMasterEnable();

    ui_clearScreen();
    ui_puts(1, 1, "SYSTEM MONITOR");

    struct time t;
    rtc_get(&t);
    ui_puts(1, 3, "Up ");
    ui_putInt(up);
    ui_puts_raw(" seconds");
    ui_puts(1, 4, "CPU: ");
    ui_putInt(cpu);
    ui_puts_raw("%");
    ui_puts(1, 5, "Memory: ");
    ui_putInt(mem);
    ui_puts_raw(" KB free");
    ui_puts(1, 6, "Last updated ");
    ui_putInt(delta);
    ui_puts_raw(" second(s) ago. Watchdog ");
    ui_puts_raw(watchdogEnabled ? "ENABLED" : "DISABLED");

    for (unsigned i = 0; i < sizeof(menuOptions) / sizeof(*menuOptions); i++) {
        ui_puts(3, (2 * i) + 8, menuOptions[i]);
    }
    ui_puts(1, (2 * (menuCursor - 1)) + 8, "*");
}

void ui_redraw() {
    switch (uiState) {
    case UI_MENU:
        ui_redrawMenu();
        break;
    default:
        break;
    }
}

enum {
    ESCAPE_NONE,
    ESCAPE_ESC,
    ESCAPE_BRK
};

unsigned ui_process_menu(unsigned char c) {
    static unsigned char escape;
    switch (escape) {
    case ESCAPE_NONE:
        if (c == 0x1b)
            // Beginning escape sequence
            escape = ESCAPE_ESC;
        else
            // Not an escape. Was it ENTER though?
            if (c == '\r')
                return menuCursor;
        break;
    case ESCAPE_ESC:
        // Continuation
        if (c == '[')
            escape = ESCAPE_BRK;
        else
            escape = ESCAPE_NONE;
        break;
    case ESCAPE_BRK:
        // Finished a sequence. Identify it.
        switch (c) {
            case 'A':   // Cursor up
                if (menuCursor > 1)
                    menuCursor--;
                break;
            case 'B':   // Cursor down
                if (menuCursor < sizeof(menuOptions) / sizeof(*menuOptions))
                    menuCursor++;
                break;
        }
        escape = ESCAPE_NONE;
        ui_redraw();
    }
    return 0;
}

void ui_process() {
    int c = ROM_UARTCharGetNonBlocking(UART0_BASE);
    if (c == -1)
        return;

    unsigned selection;
    switch (uiState) {
    case UI_MENU:
        selection = ui_process_menu((unsigned char)c);
        switch (selection) {
            case 1:     // Power off
                powerButton_press(5000);
                break;
            case 2:     // Reboot
                powerButton_press(5000);
                ROM_SysCtlDelay(50000000);
            case 3:     // Power on
                powerButton_press(500);
                break;
            case 4:
                watchdogEnabled = 0;
                break;
            case 5:
                watchdogEnabled = 1;
                break;
        }
        if (selection != 0) {
            ui_redraw();
        }
        break;
    case UI_SHELL:
        //host_shellPut(c);
        break;
    default:
        // Uh...
        uiState = UI_MENU;
        break;
    }
}

void ui_shellTerminated() {
    uiState = UI_MENU;
    ui_redraw();
}

void ui_updateStatus(unsigned mem, unsigned cpu, unsigned rUptime) {
    memoryFree = mem;
    cpuUsage = cpu;
    uptime = rUptime;
    lastUpdate = seconds;
}
