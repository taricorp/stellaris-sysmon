#include <stdarg.h>
#include <stdint.h>

#include <inc/hw_types.h>
#include <inc/hw_memmap.h>
#include <driverlib/gpio.h>
#include <driverlib/rom.h>
#include <driverlib/sysctl.h>
#include <driverlib/uart.h>

void debug_putc(const char c) {
    ROM_UARTCharPut(UART0_BASE, c);
}

void debug_puts(const char *s) {
    unsigned char c;
    while ((c = *s++) != 0) {
        debug_putc(c);
    }
}

static void _format_unsigned(unsigned x) {
    int log10 = 0; // Number of digits neeeded
    int xt = x;
    while (xt > 0) {
        log10++;
        xt /= 10;
    }

    do { // Defer comparison to ensure 0 gets a digit
        int xpow = 1, i;
        for (i = log10; i > 1; i--) { // 10^log10
            xpow *= 10;
        }
        char digit = (x / xpow) % 10;
        digit += '0';
        debug_putc(digit);
    } while (--log10 > 0);
}

static void _format_hex(unsigned x) {
    debug_puts("0x");
    // This will be wrong on non-32-bit platforms
    int i;
    const char chars[] = "0123456789ABCDEF";
    for (i = 7; i >= 0; i--) {
        debug_putc(chars[(x >> (4 * i)) & 0xF]);
    }
}

static void _vfprintf(va_list ap, const char *fmt) {
    char c;
    while ((c = *fmt++) != 0) {
        if (c == '%') {
            // Escape
            c = *fmt++;
            switch(c) {
                case 'i':
                    _format_unsigned(va_arg(ap, unsigned));
                    break;
                case 'x':
                    _format_hex(va_arg(ap, unsigned));
                    break;
                case 's':
                    debug_puts(va_arg(ap, const char *));
                    break;
                default:
                    debug_putc('!');
                    break;
            }
        } else {
            // Literal
            debug_putc(c);
        }
    }
}

void debug_printf(const char *fmt, ...) {
    va_list ap;
    va_start(ap, fmt);
    _vfprintf(ap, fmt);
    va_end(ap);
}

unsigned char debug_getc() {
    unsigned char c = ROM_UARTCharGet(UART0_BASE);
    debug_putc(c);
    return c;
}

unsigned int debug_getHex() {
    unsigned int r = 0;
    for (int i = 0; i < 8; i++) {
        unsigned char c = debug_getc();
        if (c >= '0' && c <= '9') {
            c -= '0';
        } else if (c >= 'a' && c <= 'f') {
            c -= 'a' + 10;
        } else {
            break;
        }
        r <<= 4;
        r |= c;
    }
    return r;
}

void debug_init() {
    // TODO use UART1 for this instead- conflicts with the UI.
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
    ROM_GPIOPinConfigure(GPIO_PA0_U0RX);
    ROM_GPIOPinConfigure(GPIO_PA1_U0TX);
    ROM_GPIOPinTypeUART(GPIO_PORTA_BASE, 3);
    ROM_UARTClockSourceSet(UART0_BASE, UART_CLOCK_SYSTEM);
    ROM_UARTConfigSetExpClk(UART0_BASE,
                            ROM_SysCtlClockGet(),
                            115200,
                            UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE);
    ROM_UARTEnable(UART0_BASE);

    // VT100 Erase Screen <Esc>[2J
    debug_puts("\x1b[2J");
    // Home cursor
    debug_puts("\x1b[H");
}

