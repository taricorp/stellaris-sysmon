// ENC28J60 MAC/PHY definitions
#ifndef _ENC28J60_H
#define _ENC28J60_H

// Opcodes
#define OP_RCR 0
#define OP_RBM 0x20
#define OP_WCR 0x40
#define OP_WBM 0x60
#define OP_BFS 0x80
#define OP_BFC 0x90
#define OP_SRC 0xE0

// Registers
#define ERDPTL 0
#define ERDPTH 1
#define EWRPTL 2
#define EWRPTH 3
#define ETXSTL 4
#define ETXSTH 5
#define ETXNDL 6
#define ETXNDH 7
#define ERXNDL 0xA
#define ERXNDH 0xB
#define ERXRDPTL 0xC
#define ERXRDPTH 0xD

#define ERXFCON 0x38
#define BCEN 1
#define MCEN 2
#define HTEN 4
#define MPEN 8
#define PMEN 0x10
#define CRCEN 0x20
#define ANDOR 0x40
#define UCEN 0x80

#define EPKTCNT 0x39

// Registers common to every bank
#define EIE 0x1B
#define EIR 0x1C

#define ESTAT 0x1D
#define CLKRDY 1
#define TXABRT 2
#define RXBUSY 3
#define LATECOL 0x10
#define BUFER 0x40
#define INT 0x80

#define ECON2 0x1E
#define VRPS 8
#define PWRSV 0x20
#define PKTDEC 0x40
#define AUTOINC 0x80

#define ECON1 0x1F
#define BSEL(x) (x)
#define RXEN 4
#define TXRTS 8
#define CSUMEN 0x10
#define DMAST 0x20
#define RXRST 0x40
#define TXRST 0x80

// MAC registers
#define MACON1 0x40
#define MARXEN 1
#define PASSALL 2
#define RXPAUS 4
#define TXPAUS 8

#define MACON3 0x42
#define FULDPX 1
#define FRMLNEN 2
#define HFRMEN 4
#define PHDREN 8
#define TXCRCEN 0x10
#define PADCFG(x) (x << 5)

#define MACON4 0x43
#define NOBKOFF 0x10
#define BPEN 0x20
#define DEFER 0x40

#define MABBIPG 0x44

#define MAIPGL 0x46
#define MAIPGH 0x47

#define MAMXFLL 0x4A
#define MAMXFLH 0x4B

#define MICMD 0x52
#define MIIRD 1
#define MIISCAN 2

#define MIREGADR 0x54
#define MIWRL 0x56
#define MIWRH 0x57
#define MIRDL 0x58
#define MIRDH 0x59

#define MAADR5 0x60
#define MAADR6 0x61
#define MAADR3 0x62
#define MAADR4 0x63
#define MAADR1 0x64
#define MAADR2 0x65
#define MISTAT 0x6A

#define EREVID 0x72

// PHY registers (through MIREGADR, MICMD), 16 bits each
#define PHCON1 0
#define PDPXMD 0x100

#define PHSTAT2 0x11
#define DPXSTAT 0x200
#define LSTAT 0x400

// Functions
void enc_init();
void enc_reset();

int enc_linkIsUp();

void enc_txFlush();
void enc_write(void *buf, size_t n);
void enc_writeSingle(const uint8_t x);
void enc_txCommit();

int enc_rxPending();
uint16_t enc_read(void *buf);

#endif // _ENC28J60_H
