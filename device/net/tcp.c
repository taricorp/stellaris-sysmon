#include <alloca.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "net/enet.h"
#include "net/ip.h"
#include "debug.h"

struct tcp_session {
    ipaddr_t localHost;
    ipaddr_t remoteHost;
    uint16_t localPort;
    uint16_t remotePort;
    unsigned state;
    uint32_t rxSeqNum;
    uint32_t txSeqNum;
};

// Session status
enum {
    CLOSED = 0,
    LISTEN,
    SYN_SENT,
    SYN_RECEIVED,
    ESTABLISHED,
    FIN_WAIT_1,
    FIN_WAIT_2,
    CLOSE_WAIT,
    CLOSING,
    LAST_ACK,
};

struct tcp_hdr {
    uint16_t srcport;
    uint16_t dstport;
    uint32_t seqnum;
    uint32_t acknum;
    uint16_t flags;     // And data offset
    uint16_t winsize;
    uint16_t csum;
    uint16_t urgptr;
};

// TCP header flags
#define FIN 1
#define SYN 2
#define RST 4
#define PSH 8
#define ACK 0x10
#define URG 0x20
#define ECE 0x40
#define CWR 0x80
#define NS 0x100

static struct tcp_session session;

static void tcp_sendRaw(uint16_t flags, uint32_t acknum, const void *data, size_t n) {
    // TODO acknum as parameter here is stupid.
    size_t txLen = sizeof(struct tcp_hdr) + n;
    struct tcp_hdr *txBuf = alloca(txLen);
    txBuf->srcport = session.localPort;
    txBuf->dstport = session.remotePort;
    txBuf->seqnum = htonl(session.rxSeqNum);
    txBuf->acknum = htonl(acknum);
    txBuf->flags = htons(flags | 0x5000);
    txBuf->winsize = htons(512);
    txBuf->urgptr = 0;
    memcpy(((void *)txBuf) + sizeof(*txBuf), data, n);
    ip_tx(IP_PROTO_TCP, ntohl(session.remoteHost), txBuf, txLen);
}

void tcp_handlePacket(uint16_t sz, ipaddr_t remote) {
    debug_printf("TCP: handling packet of length %i\r\n", (unsigned)sz);
    struct tcp_hdr h;
    if (sz < sizeof(h)) {
        debug_puts("Payload too small to be valid TCP\r\n");
        return;
    }
    enet_read(&h, sizeof(h));

    // Grab options and ignore them
    size_t options_len = ((ntohs(h.flags) >> 12) * 4) - sizeof(h);
    uint8_t *options = alloca(options_len);
    enet_read(options, options_len);

    // Verify source and destination are valid for this session if it's open
    if (session.state != CLOSED && session.state != LISTEN) {
        if (session.remoteHost != remote
            || session.remotePort != h.srcport
            || session.localPort != h.dstport) {
            debug_printf("Ignoring packet from %x; session is open\r\n", remote);
        }
    }

    // Handle packet
    switch (session.state) {
        case CLOSED:
        case LISTEN:
            // SYN -- client connecting
            if ((ntohs(h.flags) & SYN) == 0 || ntohs(h.flags) & (ACK | RST | FIN)) {
                debug_puts("TCP: LISTEN wonders what this garbage is..\r\n");
                break;
            }
            // Check dstport (should be 23, Telnet)
            if (ntohs(h.dstport) != 23) {
                debug_puts("TCP: ACCEPT rejects connection to non-telnet port\r\n");
                break;
            } else {
                session.localPort = h.dstport;
                session.remotePort = h.srcport;
            }
            // Set sequence number (=A)
            session.rxSeqNum = ntohl(h.seqnum) + 1;
            session.txSeqNum = ntohl(h.seqnum);    // TODO this should be pseudorandomly selected..
            // Send SYN/ACK
            session.remoteHost = remote;
            tcp_sendRaw(SYN | ACK, session.rxSeqNum, NULL, 0);
            session.state = SYN_RECEIVED;

            debug_printf("TCP LISTEN: SYN received from %x\r\n", session.remoteHost);
            break;
        case SYN_RECEIVED:
            // ACK -- connection confirmed
            // seqnum is A + 1
            // state = ESTABLISHED
            break;
        case ESTABLISHED:
            if (ntohs(h.flags) & FIN) {
                // Send ACK
                // state = CLOSE_WAIT (not used, so skipped)
                // Send FIN
                // state = LAST_ACK
            } else {
                // Data!
            }
            break;
        case LAST_ACK:
            // ACK - connection closed
            // state = CLOSED
            break;
        default:
            debug_puts("TCP state machine in unexpected state!\r\n");
            break;
    }
}

