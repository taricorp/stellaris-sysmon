#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <inc/hw_types.h>
#include <inc/hw_memmap.h>

#include <driverlib/rom.h>
#include <driverlib/gpio.h>
#include <driverlib/sysctl.h>

#include "debug.h"
#include "enc28j60.h"
#include "../spi.h"

#define RXBUF_START 0
#define RXBUF_END 0x17FF

#define TXBUF_START 0x1800
#define TXBUF_END 0x1FFF

// Active register bank
static uint8_t controlBank = 0;

/*
 * Raw "Write control register" to address.
 * addr must be a 5-bit value in the current bank.
 */
static void enc_writeControl_raw(uint8_t addr, uint8_t value) {
    uint8_t cmd[2] = {addr | OP_WCR, value};
    SPI_burst(cmd, cmd, 2);
}

/*
 * Raw "Read control register" from address, which must be a 5-bit value
 * within the current bank.
 */
static uint8_t enc_readControl_raw(uint8_t addr) {
    uint8_t cmd[2] = {addr | OP_RCR, 0};
    SPI_burst(cmd, cmd, 2);
    return cmd[1];
}

/*
 * Ensure the active control register bank is suitable for accessing the
 * specified address.
 */
static void enc_setBank(uint8_t addr) {
    // These regs are mirrored in every bank
    if (addr >= EIE && addr <= ECON1) {
        return;
    } else {
        uint8_t bank = addr >> 5;
        if (bank != controlBank) {
            controlBank = bank;
            // Twiddle ECON1:BSEL[0:1]
            uint8_t econ1 = enc_readControl_raw(ECON1);
            enc_writeControl_raw(ECON1, (econ1 & 0xFC) | bank);
        }
    }
}

/*
 * Write `value` to the control register at address `addr`.
 */
static void enc_writeControl(uint8_t addr, uint8_t value) {
    enc_setBank(addr);
    enc_writeControl_raw(addr & 0x1F, value);
}

/*
 * Read the value from the control register at address `addr`. Only for
 * MAC/MII registers. Normally use enc_readControl().
 */
static uint8_t enc_readControlM(uint8_t addr) {
    enc_setBank(addr);
    // M-register reads have a dummy byte
    uint8_t buf[3];
    buf[0] = (addr & 0x1F) | OP_RCR;
    SPI_burst(buf, buf, 3);
    return buf[2];
}

/*
 * Write `value` to the PHY register at `addr`.
 */
static void enc_writePHY(uint8_t addr, uint16_t value) {
    enc_writeControl(MIREGADR, addr);
    enc_writeControl(MIWRL, value & 0xFF);
    enc_writeControl(MIWRH, (value >> 8) & 0xFF);
    // Poll until transaction completes
    while (enc_readControlM(MISTAT) & 1);
}

static uint16_t enc_readPHY(uint8_t addr) {
    enc_writeControl(MIREGADR, addr);
    enc_writeControl(MICMD, MIIRD);
    while (enc_readControlM(MISTAT) & 1); // Poll BUSY
    enc_writeControl(MICMD, 0);
    uint16_t res = enc_readControlM(MIRDH) << 8;
    res |= enc_readControlM(MIRDL);
    return res;
}

/*
 * Read the value from the control register at address `addr`.
 *
 * MAC and MII registers (those whose names are prefixed with MA or MI) must
 * be read with enc_readControlM() instead.
 */
static uint8_t enc_readControl(uint8_t addr) {
    // TODO intelligently select mode when given MAC/MII regs
    enc_setBank(addr);
    return enc_readControl_raw(addr & 0x1F);
}

/*
 * Resets the ENC28J60 via dedicated ~RST line on PA6.
 */
void enc_reset() {
    ROM_GPIOPinWrite(GPIO_PORTA_BASE, 0x40, 0);
    // Wait (minimum 400 ns)
    // XXX assuming 50 MHz CPU clock
    ROM_SysCtlDelay(20);    // this is about 1200 ns @ 50 MHz
    ROM_GPIOPinWrite(GPIO_PORTA_BASE, 0x40, 0x40);
    // Clear cached state
    controlBank = 0;
}

void enc_init(const uint8_t MACADDR[6]) {
    // Configure SPI, 10 MHz 8-bit transfers
    // ENC28J60 Errata 1: SPI clock must be >8 MHz, otherwise MAC accesses
    // may be unreliable.
    SPI_init(10000000, 8);
    // Configure ~RST on PA6
    ROM_GPIOPinTypeGPIOOutput(GPIO_PORTA_BASE, 0x40);
    // Reset the controller
    enc_reset();
    controlBank = 0;

    // Hardware sanity checks
    debug_printf("ENC28J60 revision %i\r\n", enc_readControl(EREVID));
    if (enc_readControl(EREVID) != 6) {
        debug_puts("FAULT\r\nWrong revision. Check SPI.");
        while (1);
    }

    // Init sequence as in ENC28J60 datasheet chapter 6
    // 6.1 Set receive buffer parameters
    // Regions: 0-0x17ff: rx buffer, 0x1800-0x1FFF: tx buffer
    // Errata 5: nonzero ERXST can corrupt internal state.
    enc_writeControl(ERXNDL, 0xFF);     // TODO RXBUF_END
    enc_writeControl(ERXNDH, 0x17);
    enc_writeControl(ERXRDPTL, 0xFF);
    enc_writeControl(ERXRDPTH, 0x17);
    enc_writeControl(ERDPTL, 0);        // TODO RXBUF_START
    enc_writeControl(ERDPTH, 0);

    // 6.3 Set receive filters
    //enc_writeControl(ERXFCON, UCEN | CRCEN);
    // TODO couldn't get receive filters to work properly
    enc_writeControl(ERXFCON, 0);
    
    // 6.4 Wait for MAC/PHY oscillator ready
    while (!(enc_readControl(ESTAT) & CLKRDY));

    // 6.5 MAC initialization
    // Half-duplex, because the controller autonegotiates to that.
    enc_writeControl(MACON1, MARXEN);
    enc_writeControl(MACON3, PADCFG(1) | TXCRCEN | FRMLNEN);
    enc_writeControl(MACON4, DEFER);
    enc_writeControl(MAMXFLL, 1518 & 0xFF);
    enc_writeControl(MAMXFLH, (1518 >> 8) & 0xFF);
    enc_writeControl(MABBIPG, 0x12);
    enc_writeControl(MAIPGL, 0x12);
    enc_writeControl(MAIPGH, 0x0C);
    // Set MAC address
    for (unsigned i = 0; i < 6; i++) {
        // Ordering of these registers is insane
        const uint8_t regMap[] = {MAADR1, MAADR2, MAADR3,
                                  MAADR4, MAADR5, MAADR6};
        enc_writeControl(regMap[i], MACADDR[i]);
    }

    // 6.6 PHY initialization
    // (nothing to do)

    // Misc other configuration
    // Because we're lazy, ETXST is fixed
    enc_writeControl(ETXSTL, 0x00);
    enc_writeControl(ETXSTH, 0x18);
    // Autoincrement read and write pointers
    enc_writeControl(ECON2, AUTOINC);

    // Sanity checks
    debug_puts("MAC address: ");
    uint8_t MACReadBack[6];
    for (unsigned i = 0; i < 6; i++) {
        const uint8_t regmap[] = {MAADR1, MAADR2, MAADR3,
                                  MAADR4, MAADR5, MAADR6};
        MACReadBack[i] = enc_readControlM(regmap[i]);
    }
    enet_debug_addr(MACReadBack);
    debug_puts("\r\n");

    debug_puts("Waiting for link to come up..\r\n");
    while (!enc_linkIsUp());
    debug_printf("Link up, %s-duplex.\r\n", enc_readPHY(PHSTAT2) & DPXSTAT ? "full" : "half");

    // Enable reception
    enc_writeControl(ECON1, enc_readControl(ECON1) | RXEN);
}

int enc_linkIsUp() {
    return enc_readPHY(PHSTAT2) & LSTAT;
}

/*
 * Return 0 if the controller is busy transmitting, nonzero otherwise.
 */
int enc_txReady() {
    return !(enc_readControl(ECON1) & TXRTS);
}

static uint16_t txPktLen;

/*
 * Flush the controller's transmit buffer; reset the buffer write pointer
 * and wait for any active transmission to finish.
 */
void enc_txFlush() {
    // Reset write pointer
    enc_writeControl(EWRPTL, 0x00);
    enc_writeControl(EWRPTH, 0x18);
    txPktLen = 0;
    // Wait for active transmission to finish
    while (!enc_txReady());
}

/*
 * Write a single byte to the controller's transmit buffer.
 */
void enc_writeSingle(const uint8_t x) {
    txPktLen++;
    uint8_t cmd[2] = {OP_WBM | 0x1A, x};
    SPI_burst(cmd, cmd, 2);
}

void enc_write(void *buf, size_t n) {
    // XXX do a SPI burst without reception to allow const buffer
    txPktLen += n;

    SPI_begin();
    SPI_byte(OP_WBM | 0x1A);
    SPI_burstTail(buf, buf, n);
}

/*
 * Commit the current packet for transmit.
 *
 * Updates the TX end pointer and starts transmission.
 */
void enc_txCommit() {
    uint16_t txend = 0x1800 + txPktLen;
    enc_writeControl(ETXNDL, txend & 0xFF);
    enc_writeControl(ETXNDH, (txend >> 8) & 0xFF);
    // TODO interrupts

    // Errata 12: Previous TX error may cause TX logic to latchup.
    // Workaround: reset TX logic before all transmissions.
    uint8_t econ1 = enc_readControl(ECON1);
    enc_writeControl(ECON1, econ1 | TXRST);
    enc_writeControl(ECON1, econ1 & (~TXRST));
    // XXX BAD BAD BAD. Use BFC commands on interrupt registers.
    enc_writeControl(EIR, 0);

    // Begin transmit
    // TODO use BFS/BFC commands
    enc_writeControl(ECON1, enc_readControl(ECON1) | TXRTS);
}

int enc_rxPending() {
    return enc_readControl(EPKTCNT);
}

static void frameStatus_decode(uint16_t status) {
    const char *strings[] = {
        "LONG_DROP",
        "RES1",
        "CEVENT",
        "RES2",
        "CRCERR",
        "LENERR",
        "LRANGE",
        "OK",
        "MULTICAST",
        "BROADCAST",
        "DRIBBLE",
        "CONTROL",
        "PAUSE",
        "UOPCODE",
        "VLAN",
        "ZERO"
    };
    debug_printf("Frame status: %x", (unsigned)status);
    for (int i = 0; i < 16; i++) {
        if (status & (1 << i)) {
            debug_putc(' ');
            debug_puts(strings[i]);
        }
    }
    debug_puts("\r\n");
}

// XXX TERRIBLE. Split this to a readHeader function and allow incremental streaming.
uint16_t enc_read(void *buf) {
    // Begin RBM operation
    SPI_begin();
    SPI_byte(OP_RBM | 0x1A);
    // Next packet pointer
    uint16_t nextPkt;
    SPI_burstMid(&nextPkt, &nextPkt, 2);    // Endianness okay?
    // Status vector 0..15 (byte count)
    uint16_t n;
    SPI_burstMid(&n, &n, 2);
    // Status vector 16..31 (flags)
    uint16_t statusH;
    SPI_burstMid(&statusH, &statusH, 2);

    // Discard FCS
    n -= 4;
    // Read payload
    if (n > 1500) {
        debug_printf("enet_read: aborted large read of %i bytes\r\n", n);
        n = 0;
    }
    SPI_burstTail(buf, buf, n);

    // Decrement EPKTCNT
    // TODO use a BFS command
    enc_writeControl(ECON2, enc_readControl(ECON2) | PKTDEC);
    // Update read pointer for next packet
    enc_writeControl(ERDPTL, nextPkt & 0xFF);
    enc_writeControl(ERDPTH, (nextPkt >> 8) & 0xFF);
    // Free RX buffer space
    // Errata 14: writing even values to ERXRDPT may corrupt the buffer
    uint16_t erxrdpt;
    if (nextPkt == RXBUF_START) {
        erxrdpt = RXBUF_END;
    } else {
        erxrdpt = nextPkt - 1;
    }
    enc_writeControl(ERXRDPTL, erxrdpt & 0xFF);
    enc_writeControl(ERXRDPTH, (erxrdpt >> 8) & 0xFF);

    // Check status (received OK?)
    if (statusH & (1 << 7)) {
        return n;
    } else {
        frameStatus_decode(statusH);
        return 0;
    }
}
