#include <stdint.h>

typedef uint32_t ipaddr_t;

/*
 * IP protocol numbers. Initially specified by RFC 739, maintained by IANA.
 */
#define IP_PROTO_ICMP 1
#define IP_PROTO_TCP 6
#define IP_PROTO_UDP 17

void ip_tx(uint8_t protocol, ipaddr_t destination, void *data, size_t n);
