#include <stddef.h>
#include <stdint.h>

#define ETYPE_IP 0x0800
#define ETYPE_ARP 0x0806
#define ETYPE_WOL 0x0842
#define ETYPE_IP6 0x86DD

struct enet_hdr {
    uint8_t dest[6];
    uint8_t src[6];
    uint16_t type;
};

uint16_t htons(uint16_t x);
uint16_t ntohs(uint16_t x);
uint32_t htonl(uint32_t x);
uint32_t ntohl(uint32_t x);

void enet_init();
const void *getLocalMACAddress();

void enet_begin(uint16_t etype, const uint8_t dest[6]);
void enet_write(void *buf, size_t n);
void enet_commit();

int enet_rxPending();
uint16_t enet_get(struct enet_hdr *h);
size_t enet_read(void *buf, size_t n);

void enet_debug_addr(const uint8_t *addr);
