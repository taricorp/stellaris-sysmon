#include <alloca.h>
#include <stdint.h>
#include <string.h>

#include "net/enet.h"
#include "net/ip.h"
#include "debug.h"

struct arp {
    uint8_t hwaddr[6];
    ipaddr_t addr;
};

// TODO static ARP for now; Homura's eth0
static struct arp arp_table[8] = {
    {
        .hwaddr = {0xf0, 0xde, 0xf1, 0e1, 0xdc, 0xf6},
        .addr = 0x0A0000001     // 10.0.0.1
    }
};

// Our IP address.
// TODO static IP for now. 10.0.0.2
static ipaddr_t IPADDR = 0x0A000002;

ipaddr_t getLocalIPAddress() {
    return IPADDR;
}

struct ip_hdr {
    // 4 MSb: IP version, 4 LSb: header length in words
    uint8_t ver_ihl;
    // 6 MSb: DiffServ, 2 LSb: congestion notification
    uint8_t dscp_ecn;
    // Total length of packet in bytes
    uint16_t len;
    // Fragment ID
    uint16_t id;
    // 2 MSb: flags, 14 LSb: fragment offset
    uint16_t fragment;
    // Time to live
    uint8_t ttl;
    // Upper-level protocol (RFC 790, IANA)
    uint8_t proto;
    // One's complement of this header
    uint16_t cksum;
    // Source node
    ipaddr_t src;
    // Destination node
    ipaddr_t dst;
};

/*
 * Resolves the hardware address of the host with ip address `dest` on the
 * local network, storing the hardware address to the 6-byte buffer pointed
 * to by `hwaddr`.
 *
 * Returns zero on success, otherwise nonzero.
 */
int arp_resolve(ipaddr_t dest, uint8_t *hwaddr) {
    for (unsigned i = 0; i < sizeof(arp_table) / sizeof(*arp_table); i++) {
        if (arp_table[i].addr == dest) {
            memcpy(hwaddr, arp_table[i].hwaddr, 6);
            return 0;
        }
    }
    // TODO actually send out an ARP request
    return 1;
}

static uint16_t ip_cksum(const void *h, size_t n) {
    uint32_t sum = 0;

    while (n > 1) {
        sum += *((const uint16_t *)h);
        // Fold if high order bit set
        if (sum & 0x80000000)
            sum = (sum & 0xFFFF) + (sum >> 16);
        h += 2;
        n -= 2;
    }
    // Trailing lone byte
    if (n)
        sum += *((const uint8_t *)h);

    while (sum >> 16)
        sum = (sum & 0xFFFF) + (sum >> 16);

    return ~sum;
}

/*
 * Transmits an IP packet with the specified upper-level protocol and
 * destination node, with contents of `n` bytes read from `data`.
 *
 * Protocol is one of the IP_PROTO_* constants.
 */
void ip_tx(uint8_t protocol, ipaddr_t destination, void *data, size_t n) {
    // Fill stuff
    struct ip_hdr header;
    memset(&header, 0, sizeof(header));
    header.ver_ihl = 0x45;      // IPv4, no options
    header.len = htons(n + 20);
    header.ttl = 64;
    header.proto = protocol;
    header.src = htonl(IPADDR);
    header.dst = htonl(destination);
    // Compute checksum
    const uint16_t *p = (const uint16_t *)&header;
    uint32_t csum = 0;
    for (unsigned i = 0; i < sizeof(header) / 2; i++) {
        csum += ntohs(p[i]);
    }
    if (csum > 0xFFFF) {
        csum += csum >> 16;
        csum &= 0xFFFF;
    }
    header.cksum = htons((uint16_t)(~csum & 0xFFFF));

    // Transmit
    uint8_t hwaddr[6];
    // XXX error handling
    arp_resolve(destination, hwaddr);
    enet_begin(ETYPE_IP, hwaddr);
    enet_write(&header, sizeof(header));
    enet_write(data, n);
    enet_commit();
}

void ip_poll_rx() {
    if (enet_rxPending() > 0) {
        struct enet_hdr h;
        uint16_t sz = enet_get(&h);
        if (memcmp(getLocalMACAddress(), h.dest, sizeof(h.dest)) == 0) {
            sz -= sizeof(h);
            // We're the target at the hardware layer
            // TODO this should really be handled at the MAC layer
            if (ntohs(h.type) == ETYPE_IP) {
                // And it's an IP frame
                struct ip_hdr ih;
                if (enet_read(&ih, sizeof(ih)) == sizeof(ih) &&
                    (ih.ver_ihl & 0xF0) == 0x40 &&
                    htonl(ih.dst) == getLocalIPAddress()) {
                    // IPv4 pointed at us
                    if ((ih.ver_ihl & 0xF) != 5) {
                        debug_puts("Unusuable IP header length\r\n");
                        return;
                    }
                    if (ntohs(ih.len) > sz) {
                        debug_printf("Bogus IP packet length: %i\r\n", ntohs(ih.len));
                        return;
                    }
                    // sz becomes number of bytes in payload
                    sz = ntohs(ih.len) - (4 * (ih.ver_ihl & 0xF));

                    uint16_t pkt_cksum = ih.cksum;
                    ih.cksum = 0;
                    debug_printf("Got %i-byte IP packet from %x, checksum %s\r\n",
                            (unsigned)sz,
                            ntohl(ih.src),
                            pkt_cksum == ip_cksum(&ih, sizeof(ih)) ? "OK" : "FAIL");

                    switch (ih.proto) {
                        case IP_PROTO_UDP:
                            debug_puts("UDP: ignoring\r\n");
                            break;
                        case IP_PROTO_TCP:
                            tcp_handlePacket(sz, ih.src);
                            break;
                        default:
                            debug_puts("Unrecognized IP proto\r\n");
                            break;
                    }
                } else {
                    debug_puts("IP header troubles..\r\n");
                }
            } else {
                debug_puts("Frame ethertype is not IP\r\n");
            }
        } else {
            debug_puts("Frame destination address not ours\r\n");
        }
    }
}

struct udp {
    uint16_t srcport;
    uint16_t dstport;
    uint16_t len;
    uint16_t cksum;
    uint8_t data[];
};

/*
void udp_sendto(ipaddr_t dest, void *data, size_t n) {
    struct udp *h = alloca(sizeof(struct udp) + 1);
    h->srcport = htons(0);
    h->dstport = htons(1);
    h->len = htons(sizeof(*h) + 1);
    h->cksum = htons(0);
    h->data[0] = 0xFF;
    ip_tx(IP_PROTO_UDP, dest, h, sizeof(struct udp) + 1);
}
*/
