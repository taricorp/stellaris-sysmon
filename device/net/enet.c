#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "debug.h"
#include "enc28j60.h"
#include "net/enet.h"

// CONFIGURATION VALUES
const uint8_t MACADDR[6] = {0xf0, 0xde, 0xf1, 0xe1, 0xdc, 0xf7};
// CONFIGURATION END

uint16_t htons(uint16_t x) {
    return ((x & 0xFF) << 8) | ((x >> 8) & 0xFF);
}

uint16_t ntohs(uint16_t x) {
    return htons(x);
}

uint32_t htonl(uint32_t x) {
    return ((x & 0xFF000000) >> 24) |
           ((x & 0x00FF0000) >> 8) |
           ((x & 0x0000FF00) << 8) |
           ((x & 0x000000FF) << 24);
}

uint32_t ntohl(uint32_t x) {
    return htonl(x);
}

// Buffer for whole Ethernet frame
static uint8_t frameBuf[1518];
// Buffered frame payload size
static uint16_t frameSize;
// Buffer read pointer
static uint16_t frameBufP;

/*
 * Initialize the ethernet layer. Must be called before any other enet_*
 * functions.
 */
void enet_init() {
    enc_init(MACADDR);
}

const void *getLocalMACAddress() {
    return MACADDR;
}

/*
 * Set up transmission of an Ethernet frame with the specified Ethertype
 * (ETYPE_*) to the node with hardware address `dest`.
 *
 * This process is stateful. In order to transmit a complete frame,
 * call enet_begin(), enet_write() zero or more times, followed by
 * enet_commit().
 */
void enet_begin(uint16_t etype, const uint8_t dest[6]) {
    // Assemble header (early so we stall less if need to wait)
    struct enet_hdr hdr;
    memcpy(hdr.dest, dest, sizeof(hdr.dest));
    memcpy(hdr.src, MACADDR, sizeof(hdr.src));
    // Stellaris is little-endian
    hdr.type = htons(etype);

    // Flush transmit buffers, prepare for new packet
    enc_txFlush();

    // ENC28J60 control byte
    // TODO set CSUMEN, etc in ECON1 rather than overriding here
    enc_writeSingle(0xF);
    // Ethernet header
    enc_write(&hdr, sizeof(hdr));
}

/*
 * Write `n` bytes from `buf` into the current frame.
 */
void enet_write(void *buf, size_t n) {
    enc_write(buf, n);
}

/*
 * Finish frame assembly, marking the current Ethernet frame as ready for
 * transmission.
 */
void enet_commit() {
    enc_txCommit();
}

/*
 * Returns the number of buffered frames pending processing.
 */
int enet_rxPending() {
    return enc_rxPending();
}

/*
 * Begin reading a received Ethernet frame by getting the header and status
 * information.
 *
 * This is stateful: it clears all data from a previously received packet.
 *
 * Returns the frame payload size in bytes.
 */
uint16_t enet_get(struct enet_hdr *h) {
    frameSize = enc_read(frameBuf);
    memcpy(h, frameBuf, sizeof(*h));
    frameBufP = sizeof(*h);

    debug_puts("enet: ");
    enet_debug_addr(h->src);
    debug_puts(" => ");
    enet_debug_addr(h->dest);
    debug_puts("\r\n");

    return frameSize;
}

/*
 * Read `n` bytes from the active Ethernet frame into `buf`, up to the frame
 * size.
 *
 * Returns the number of bytes read, which may be less than n if frame end has
 * been reached.
 */
size_t enet_read(void *buf, size_t n) {
    if (n > frameSize)
        n = frameSize;
    memcpy(buf, frameBuf + frameBufP, n);
    frameBufP += n;
    frameSize -= n;

    return n;
}

void enet_debug_addr(const uint8_t *addr) {
    const char *hcl = "0123456789ABCDEF";

    for (int i = 0; i < 6; i++) {
        uint8_t c = *addr++;
        debug_putc(hcl[(c >> 4) & 0xF]);
        debug_putc(hcl[c & 0xF]);
        if (i != 5)
            debug_putc(':');
    }
}
