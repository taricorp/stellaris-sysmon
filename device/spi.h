#ifndef _SPI_H
#define _SPI_H

#include <stdint.h>

void SPI_init(unsigned freq, unsigned width);

void SPI_begin();
uint8_t SPI_byte(uint8_t x);
void SPI_burstMid(void *txBuf, void *rxBuf, size_t n);
void SPI_burstTail(void *txBuf, void *rxBuf, size_t n);
void SPI_burst(void *txBuf, void *rxBuf, size_t sz);

#endif
