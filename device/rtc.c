#include <inc/hw_types.h>
#include <inc/hw_memmap.h>
#include <driverlib/hibernate.h>
#include <driverlib/rom.h>
#include <driverlib/sysctl.h>
#include <driverlib/timer.h>

#include "rtc.h"

static unsigned timeSeconds = 0;

static void formatTime(uint32_t d, struct time *t) {
    t->utime = d;
    t->seconds = d % 60;
    d /= 60;
    t->minutes = d % 60;
    d /= 60;
    t->hours = d % 24;
    d /= 24;
    t->days = d;
}

void rtc_init() {
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
    ROM_TimerConfigure(TIMER0_BASE, TIMER_CFG_PERIODIC);
    ROM_TimerLoadSet(TIMER0_BASE, TIMER_A, 50000000);
    ROM_TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
    ROM_TimerEnable(TIMER0_BASE, TIMER_A);
}

void rtc_get(struct time *t) {
    formatTime(timeSeconds, t);
}

void __attribute__((interrupt("IRQ"))) rtc_timer_tick() {
    timeSeconds++;
}

