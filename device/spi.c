#include <stddef.h>
#include <stdint.h>

#include <inc/hw_types.h>
#include <inc/hw_memmap.h>
#include <driverlib/gpio.h>
#include <driverlib/pin_map.h>
#include <driverlib/rom.h>
#include <driverlib/ssi.h>

#include "debug.h"

/*
 * Configures SSI0 as SPI master with clock frequency `freq` and transfers
 * of `width` bits.
 *
 * width must be in the range 4..16, inclusive.
 */
void SPI_init(unsigned freq, unsigned width) {
    // (LM4F120H5QR datasheet, section 15.4)
    // SSI0 is on PA[2:5]: configure GPIO
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    ROM_GPIOPinConfigure(GPIO_PA2_SSI0CLK);
    // Can't use SSIFss because burst transactions have long (>16-bit frames)
    ROM_GPIOPinTypeGPIOOutput(GPIO_PORTA_BASE, 0x08); // PA3 ~CS
    ROM_GPIOPinWrite(GPIO_PORTA_BASE, 0x08, 0x08);

    ROM_GPIOPinConfigure(GPIO_PA4_SSI0RX);
    ROM_GPIOPinConfigure(GPIO_PA5_SSI0TX);
    ROM_GPIOPinTypeSSI(GPIO_PORTA_BASE, 0x34);
    // Turn on the SSI
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI0);

    // Configure
    unsigned long FSSI = ROM_SysCtlClockGet();
    ROM_SSIConfigSetExpClk(SSI0_BASE, FSSI, SSI_FRF_MOTO_MODE_0,
                           SSI_MODE_MASTER, freq, width);
    // Enable
    ROM_SSIEnable(SSI0_BASE);
}

/*
 * Begin transaction (assert ~CS).
 */
void SPI_begin() {
    ROM_GPIOPinWrite(GPIO_PORTA_BASE, 0x08, 0);
}

uint8_t SPI_byte(uint8_t x) {
    unsigned long xl;
    ROM_SSIDataPut(SSI0_BASE, x);
    ROM_SSIDataGet(SSI0_BASE, &xl);
    while (ROM_SSIBusy(SSI0_BASE));
    return (uint8_t)xl;
}

void SPI_burstMid(const void *txBuf, void *rxBuf, size_t n) {
    const uint8_t *t = (const uint8_t *)txBuf;
    uint8_t *r = (uint8_t *)rxBuf;
    // XXX inefficient. Use the buffers!
    while (n-- > 0) {
        unsigned long rtx;
        ROM_SSIDataPut(SSI0_BASE, *t++);
        ROM_SSIDataGet(SSI0_BASE, &rtx);
        *r++ = (uint8_t)rtx;
    }
    // Wait for operations to complete
    while (ROM_SSIBusy(SSI0_BASE));
}

void SPI_burstTail(const void *txBuf, void *rxBuf, size_t n) {
    SPI_burstMid(txBuf, rxBuf, n);
    // Clear CS
    ROM_GPIOPinWrite(GPIO_PORTA_BASE, 0x08, 0x08);
}

/*
 * Transmits and receives a burst of bytes over the SPI interface.
 *
 * Moves `sz` bytes from `txBuf` to the connected device, receiving into
 * `rxBuf`. The buffers may be the same. ~CS is asserted through the entire
 * burst.
 *
 * Returns a copy of `rxBuf`.
 */
void SPI_burst(const void *txBuf, void *rxBuf, size_t sz) {
    SPI_begin();
    SPI_burstTail(txBuf, rxBuf, sz);
}
