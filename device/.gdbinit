# Connect to OpenOCD target, load program and reset processor
file sysmon.elf
target extended-remote :3333
monitor halt
load
monitor reset init
