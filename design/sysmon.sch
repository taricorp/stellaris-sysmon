EESchema Schematic File Version 2  date 4/3/2013 2:00:42 PM
LIBS:connectors
LIBS:tari
LIBS:sysmon-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "3 apr 2013"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LAUNCHPAD M?
U 1 1 514F8F40
P 5300 3600
F 0 "M?" H 5500 2400 60  0000 C CNN
F 1 "LAUNCHPAD" H 5300 4750 60  0000 C CNN
	1    5300 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 4650 6350 4650
Wire Wire Line
	5850 4550 6350 4550
Wire Wire Line
	5850 4450 6350 4450
Text Label 4250 3300 0    60   ~ 0
ETH_MOSI
Text Label 5900 4450 0    60   ~ 0
ETH_MISO
Text Label 5900 4550 0    60   ~ 0
ETH_~CS
Text Label 5900 4650 0    60   ~ 0
ETH_CLK
Wire Wire Line
	4250 3300 4750 3300
Wire Wire Line
	4750 2800 4250 2800
Wire Wire Line
	4750 2900 4250 2900
Text Label 4250 2800 0    60   ~ 0
HOST_D_RX
Text Label 4250 2900 0    60   ~ 0
HOST_D_TX
Wire Wire Line
	5850 4050 6350 4050
Wire Wire Line
	5850 3500 6350 3500
Text Label 5900 3500 0    60   ~ 0
HOST_D_RTS
Text Label 5900 4050 0    60   ~ 0
HOST_D_CTS
Wire Wire Line
	5850 4250 6350 4250
Text Label 5900 4250 0    60   ~ 0
DISP_~BL
Text Label 5900 3950 0    60   ~ 0
DISP_D0
Wire Wire Line
	5850 3950 6350 3950
Wire Wire Line
	5850 3200 6350 3200
Text Label 5900 3200 0    60   ~ 0
DISP_D1
Wire Wire Line
	5850 3300 6350 3300
Wire Wire Line
	5850 3400 6350 3400
Text Label 5900 3300 0    60   ~ 0
DISP_D2
Text Label 5900 3400 0    60   ~ 0
DISP_D3
Wire Wire Line
	4750 3000 4250 3000
Wire Wire Line
	4250 3100 4750 3100
Text Label 4250 3000 0    60   ~ 0
DISP_D4
Text Label 4250 3100 0    60   ~ 0
DISP_D5
Wire Wire Line
	5850 2800 6350 2800
Wire Wire Line
	5850 2900 6350 2900
Wire Wire Line
	5850 3000 6350 3000
Text Label 5900 2800 0    60   ~ 0
DISP_A0
Text Label 5900 2900 0    60   ~ 0
DISP_A1
Text Label 5900 3000 0    60   ~ 0
DISP_CS0
Text Label 5900 3100 0    60   ~ 0
DISP_CS1
Wire Wire Line
	5850 3100 6350 3100
Text Notes 4200 2150 0    60   ~ 0
HOST_D_[x] refers to the device's line of that name\nHOST_D_RX = Host -> Device
Wire Wire Line
	4750 4050 4250 4050
Text Label 4250 4050 0    60   ~ 0
DISP_~WR
Wire Wire Line
	4750 4150 4250 4150
Wire Wire Line
	4750 4250 4250 4250
Wire Wire Line
	4750 4350 4250 4350
Text Label 4250 4150 0    60   ~ 0
DISP_~CU
Text Label 4250 4250 0    60   ~ 0
DISP_CUE
Text Label 4250 4350 0    60   ~ 0
DISP_~CLR
Wire Wire Line
	5850 2700 6250 2700
Wire Wire Line
	5850 2600 6250 2600
Text Label 6050 2600 0    60   ~ 0
+5V
Text Label 6050 2700 0    60   ~ 0
GND
$Comp
L AV_HDLX-2416 U?
U 1 1 514FB10A
P 8850 2900
F 0 "U?" V 9550 3250 60  0000 C CNN
F 1 "AV_HDLX-2416" V 8450 2950 60  0000 C CNN
	1    8850 2900
	0    1    1    0   
$EndComp
$Comp
L AV_HDLX-2416 U?
U 1 1 514FB117
P 8850 4100
F 0 "U?" V 9550 4450 60  0000 C CNN
F 1 "AV_HDLX-2416" V 8450 4150 60  0000 C CNN
	1    8850 4100
	0    1    1    0   
$EndComp
Wire Wire Line
	8250 2650 7850 2650
Wire Wire Line
	8250 2750 7850 2750
Wire Wire Line
	8250 3850 7850 3850
Wire Wire Line
	8250 3950 7850 3950
Text Label 7850 2650 0    60   ~ 0
DISP_CS0
Text Label 7850 3850 0    60   ~ 0
DISP_CS1
Text Label 7850 3950 0    60   ~ 0
GND
Text Label 7850 2750 0    60   ~ 0
GND
Wire Wire Line
	8250 4050 7850 4050
Wire Wire Line
	8250 2850 7850 2850
Text Label 7850 2850 0    60   ~ 0
DISP_~CLR
Text Label 7850 4050 0    60   ~ 0
DISP_~CLR
Wire Wire Line
	8250 3450 7850 3450
Wire Wire Line
	8250 4650 7850 4650
Wire Wire Line
	9550 4650 9950 4650
Wire Wire Line
	9550 3450 9950 3450
Text Label 9750 3450 0    60   ~ 0
GND
Text Label 9750 4650 0    60   ~ 0
GND
Text Label 7850 3450 0    60   ~ 0
+5V
Text Label 7850 4650 0    60   ~ 0
+5V
Text Notes 4650 5000 0    60   ~ 0
USB device port is in use (USB0)
Wire Wire Line
	4750 3400 4250 3400
Text Label 4250 3400 0    60   ~ 0
ETH_~RST
$EndSCHEMATC
